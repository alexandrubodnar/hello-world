FROM python

COPY . /app
WORKDIR /app
RUN pip install -r requirments.txt

ENTRYPOINT ["python"]
CMD ["hello-world.py"]